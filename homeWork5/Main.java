package Dom555;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        System.out.println("Выберите сложность: 1 или 2...");
        Scanner input = new Scanner(System.in);
        int skill = input.nextInt();

        if (skill == 1) {

            int easy[] = new int[7];
            easy[0] = 1 + (int) (Math.random() * 3);
            for (int i = 1; i < easy.length; i++) {
                easy[i] = easy[i - 1] + 2;
                i++;
                easy[i] = easy[i - 1] - 1;
            }
            for (int i = 0; i < easy.length - 1; i++) {
                System.out.print(easy[i] + "  ");
            }
            System.out.print("x");
            System.out.println(" ");
            System.out.println("Продолжите последовательность ");
            for (int i = 0; i < 3; i++) {
                int answr1 = input.nextInt();
                if (answr1 == easy[6]) {
                    System.out.println("Правильно!");
                    break;
                } else {
                    if (i != 2) {
                        System.out.println("Попробуйте еще раз");
                    }
                    ;
                    if (i == 2) {
                        System.out.println("Ответ неверный.\nПравильный ответ");
                        for (int e = 0; e < easy.length; e++) {
                            System.out.print(easy[e] + "    ");
                        }
                        String t = "-1";
                        String r = "+2";
                        System.out.println("");
                        System.out.print("  " + r + "   " + t + "   " + r + "   " + t + "   " + r + "   " + t);

                    }
                }
            }
        } else {
            int hard[] = new int[9];
            hard[0] = 1 + (int) (Math.random() * 3);
            for (int i = 1; i < hard.length; i++) {
                hard[i] = hard[i - 1] + 1;
                i++;
                hard[i] = hard[i - 1] * 2;
            }
            for (int i = 0; i < hard.length - 1; i++) {
                System.out.print(hard[i] + "  ");
            }
            System.out.print("x");
            System.out.println(" ");
            System.out.println("Продолжите последовательность ");
            for (int i = 0; i < 3; i++) {
                int answr2 = input.nextInt();
                if (answr2 == hard[8]) {
                    System.out.println("Правильно!");
                    break;
                } else {
                    if (i != 2) {
                        System.out.println("Попробуйте еще раз");
                    }
                    if (i == 2) {
                        System.out.println("Ответ неверный.\nПравильный ответ");
                        for (int e = 0; e < hard.length; e++) {
                            System.out.print(hard[e] + "    ");
                        }
                        String r = "+1";
                        String t = "*2";
                        System.out.println("");
                        System.out.print("  " + r + "   " + t + "   " + r + "   " + t + "    " + r + "    " + t + "    " + r + "    " + t);

                    }
                }
            }
        }
    }
}
