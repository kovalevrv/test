package Test;

import dom7.CalcApp;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class Test {


    public static void main(String[] args) {
        String str = "2+3-1*5/2";
        String[] arr = str.split("(?=([+\\-*/]))");
        int result = Integer.parseInt(arr[0]);
        for (int i = 1; i < arr.length; i++) {
            result = performCalculation(result, arr[i]);
        }
        System.out.println(result);
    }

        public static int performCalculation(int currentResult, String operationAndNumber) {
            String[] operationAndNumberArr = operationAndNumber.split("");
            String operation = operationAndNumberArr[0];
            int number = Integer.parseInt(operationAndNumberArr[1]);
            switch (operation) {
                case "+":
                    return currentResult + number;
                case "-":
                    return currentResult - number;
                case "*":
                    return currentResult * number;
                case "/":
                    return currentResult / number;
                default:
                    throw new UnsupportedOperationException("Unsupported operation!");
            }
        }
    }