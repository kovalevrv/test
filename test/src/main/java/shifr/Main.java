package shifr;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Encoder encoder = new Encoder();
        Decoder decoder = new Decoder();

        System.out.println("Введите сообщение");
        Scanner input1 = new Scanner(System.in);
        String str = input1.nextLine();

        System.out.println("Для   шифрования сообщения нажмите 1");
        System.out.println("Для ДЕшифрования сообщения нажмите 2");
        int enc = input1.nextInt();

        if (enc == 1) {
             encoder.encoder1();
            char[] shifr1 = new char[str.length()];
            for (int i = 0; i < str.length(); i++) {
                shifr1[i] = (char) ((int) (str.charAt(i)) + encoder.getEncoderKey());
            }
            System.out.print("Ваше зашифрованное сообщение:  ");
            System.out.println(shifr1);
        } else if (enc == 2) {
            decoder.decoder1();
            char[] shifr2 = new char[str.length()];
            for (int i = 0; i < str.length(); i++) {
            shifr2[i] = (char) ((int) (str.charAt(i)) - decoder.getDecoderKey());
            }
            System.out.print("Ваше ДЕшифрованное сообщение:  ");
            System.out.println(shifr2);
        } else {
            System.out.println("Вы ввели что то другое...");
        }
    }
}


