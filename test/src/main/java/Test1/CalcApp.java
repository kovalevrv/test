package Test1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class CalcApp {

    private static int pos;
    private StringTokenizer tokens;
    public String[] strings2;

    public CalcApp(String str) {

        this.tokens = new StringTokenizer(str, "+-/*()", true);
        List<String> list = new ArrayList<>();
        while (tokens.hasMoreTokens())
            list.add(tokens.nextToken());
        strings2 = list.toArray(new String[0]);
    }

    public static void main(String[] args) {
        System.out.println("Введите пример:  ");
        Scanner name1 = new Scanner(System.in);
        String str = name1.nextLine();
        CalcApp calcApp = new CalcApp(str);
        System.out.println("Ответ: "+calcApp.calculate());
    }

    public double calculate() {
        double first = multiply();
        while (pos < strings2.length) {
            String operator = strings2[pos];
            if (!operator.equals("+") && !operator.equals("-")) {
                break;
            } else {
                pos++;
            }
            double second = multiply();
            if (operator.equals("+")) {
                first += second;
            } else {
                first -= second;
            }
        }
        return first;
    }

    public double multiply() {
        double first = factor();
        while (pos < strings2.length) {
            String operator = strings2[pos];
            if (!operator.equals("*") && !operator.equals("/")) {
                break;
            } else {
                pos++;
            }
            double second = factor();
            if (operator.equals("*")) {
                first *= second;
            } else {
                first /= second;
            }
        }
        return first;
    }

    public double factor() {
        String next = strings2[pos];
        double result;
        if (next.equals("(")) {
            pos++;
            result = calculate();
            String closingBracket;
            if (pos < strings2.length) {
                closingBracket = strings2[pos];
            } else {
                throw new IllegalArgumentException("ошибка");
            }
            if (closingBracket.equals(")")) {
                pos++;
                return result;
            }
            throw new IllegalArgumentException("ошибка");
        }
        pos++;
        return Double.parseDouble(next);
    }
}