package dom6;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {

        System.out.println("Введите пример");
        Scanner name1 = new Scanner(System.in);
        String str = name1.nextLine();
        str = str.replaceAll("\\s+", "");
        StringTokenizer st = new StringTokenizer(str, " +-*/", true);
        String str1 = st.nextToken();
        String str2 = st.nextToken();
        String str3 = st.nextToken();
        while (st.hasMoreTokens()) ;
        int pervoe = Integer.parseInt(str1);
        int vtoroe = Integer.parseInt(str3);
        double result = 0;
        if (str2.equals("+")) result = pervoe + vtoroe;
        if (str2.equals("-")) result = pervoe - vtoroe;
        if (str2.equals("*")) result = pervoe * vtoroe;
        if (str2.equals("/")) result = pervoe / vtoroe;
        System.out.print("Ответ:  ");
        System.out.println(result);

    }
}


